﻿#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <chrono>
#include <memory>

#include <opencv2/opencv.hpp>

namespace bi = boost::interprocess;

template <typename T>
T GetCommandLineOption(int argc, char** argv, const std::string& key, const T default_val) {
  const auto begin = argv;
  const auto end = argv + argc;
  auto** itr = std::find(begin, end, key);
  if (itr != end && ++itr != end) {
    return *itr;
  }
  return default_val;
}

template <>
float GetCommandLineOption(int argc, char** argv, const std::string& key, const float default_val) {
  const auto s = GetCommandLineOption<char*>(argc, argv, key, nullptr);
  return s == nullptr ? default_val : std::stof(s);
}

template <>
int GetCommandLineOption(int argc, char** argv, const std::string& key, const int default_val) {
  const auto s = GetCommandLineOption<char*>(argc, argv, key, nullptr);
  return s == nullptr ? default_val : std::stoi(s);
}

template <>
bool GetCommandLineOption(int argc, char** argv, const std::string& key, const bool default_val) {
  const auto begin = argv;
  const auto end = argv + argc;
  return std::find(begin, end, key) == end ? default_val : !default_val;
}


struct BaseAllocator {
    virtual char * data() = 0;
    virtual ~BaseAllocator();
};

BaseAllocator::~BaseAllocator(){}

struct MemoryMappedFileAllocator : public BaseAllocator {

MemoryMappedFileAllocator(const std::string& file_name, const std::size_t file_size) : file_name_(file_name){
  bi::file_mapping::remove(file_name.c_str());
  std::filebuf fbuf;
  fbuf.open(file_name, std::ios_base::in | std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
  fbuf.pubseekoff(file_size-1, std::ios_base::beg);
  fbuf.sputc(0);

  m_file_ = bi::file_mapping(file_name_.c_str(), bi::read_write);
  region_ = bi::mapped_region(m_file_, bi::read_write, 0, file_size);
  std::cout <<"Got memory region of size " << region_.get_size() << std::endl;
}

 ~MemoryMappedFileAllocator() override { bi::file_mapping::remove(file_name_.c_str()); }

 char* data() override {
   return static_cast<char*>(region_.get_address());
 }

 const std::string file_name_;
 bi::file_mapping m_file_;
 bi::mapped_region region_;

};

struct InMemoryAllocator : public BaseAllocator {
InMemoryAllocator(const size_t size){
  data_ = new char[size];
}

 char* data() override {
   return data_;
 }

 char * data_;
};


int main(int argc, char *argv[])
{
  const std::string file_name  =  GetCommandLineOption(argc, argv, "-f", "file.bin");
  const size_t width = static_cast<size_t>(GetCommandLineOption(argc, argv, "-w", 90000));
  const size_t height = static_cast<size_t>(GetCommandLineOption(argc, argv, "-h", 90000));
  const std::size_t file_size = width * height;
  const auto v_length = GetCommandLineOption(argc, argv, "-v", 500);
  const auto u_length = GetCommandLineOption(argc, argv, "-u", 1);
  const auto trials = GetCommandLineOption(argc, argv, "-n", 100);
  const auto in_memory = GetCommandLineOption(argc, argv, "-i", false);

  std::shared_ptr<BaseAllocator> alloc = in_memory ? std::dynamic_pointer_cast<BaseAllocator>(std::make_shared<InMemoryAllocator>(file_size))
                                                   : std::dynamic_pointer_cast<BaseAllocator>(std::make_shared<MemoryMappedFileAllocator>(file_name, file_size));

  cv::Mat image(cv::Size(width, height), CV_8UC1, alloc->data(), cv::Mat::AUTO_STEP);

  auto total_elapsed = 0.0;
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> dist_u(0, width - u_length);
  std::uniform_int_distribution<std::mt19937::result_type> dist_v(0, height -v_length);

  for (auto i = 0; i < trials; ++i) {
    auto x = dist_u(rng);
    auto y = dist_v(rng);
    auto start  = std::chrono::high_resolution_clock::now();
    auto im = image(cv::Rect(x, y, u_length, v_length));
    im.setTo(78);
    total_elapsed += static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count()) / 1000.0;
  }
  std::cout << "Average time over " << trials << "trials: " <<  total_elapsed / trials << " millisecond" << std::endl;

  return 0;
}
